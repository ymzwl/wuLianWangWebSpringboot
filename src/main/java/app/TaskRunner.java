package app;

import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

/**
 * 初始化一个测试Demo任务
 * 创建者 科帮网
 * 创建时间	2018年4月3日
 */
@Component
public class TaskRunner implements ApplicationRunner{
    
	private final static Logger LOGGER = LoggerFactory.getLogger(TaskRunner.class);
	
	@Autowired @Qualifier("Scheduler")
    private Scheduler scheduler;
	
	@Override
    public void run(ApplicationArguments var) throws Exception{
    		LOGGER.info("初始化测试任务");
    		/*
    		String myJobClass="app.job.MyJob";
    		String myJobName="myJobName";
    		String myJobGroup="myJobGroup";
    		String myJobDescription="myJobDescription";
    		String myJobExpression="0/1 * * * * ?";
    		
			Class cls = Class.forName(myJobClass) ;
   	        cls.newInstance();
   	        //构建job信息
			JobDetail job = JobBuilder.newJob(cls).withIdentity(myJobName,myJobGroup).withDescription(myJobDescription).build();
   	        // 触发时间点
   	        CronScheduleBuilder cronScheduleBuilder = CronScheduleBuilder.cronSchedule(myJobExpression);
   	        Trigger trigger = TriggerBuilder.newTrigger().withIdentity("trigger"+myJobName, myJobGroup).startNow().withSchedule(cronScheduleBuilder).build();
   	        
   	        //交由Scheduler安排触发
   	        scheduler.scheduleJob(job, trigger);
   	        */
    }

}