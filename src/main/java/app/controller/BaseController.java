package app.controller;

import java.io.OutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import app.util.Util;



public class BaseController {
	public void outJson(HttpServletResponse response,String json){
		try {
			response.setContentType("application/json; charset=utf-8");  
	        response.setCharacterEncoding("UTF-8");
	        OutputStream out = response.getOutputStream();
	        out.write(json.getBytes("UTF-8"));  
	        out.flush();  
	        out.close();
            Util.outLog(json, new Exception());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void outString(HttpServletResponse response,String json){
		try {
	        response.setCharacterEncoding("UTF-8");
	        OutputStream out = response.getOutputStream();  
	        out.write(json.getBytes("UTF-8"));  
	        out.flush();  
	        out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
