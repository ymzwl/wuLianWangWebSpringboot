package app.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import app.dao.CommonDao;
import app.util.HttpUtil;


@Controller
@RequestMapping("/common")
public class CommonController extends BaseController{
	
	@Resource
	private CommonDao commonDao;
	
	
	
	/**
	 * @param request
	 * @param response
	 * @return
	 * 执行sql返回json
	 */
	@RequestMapping("/listSql")
	public String listSql(HttpServletRequest request,HttpServletResponse response){
		try {
//			String data=request.getReader().readLine();
			String sql=request.getParameter("sql");
			//JSONObject dataJson=JSONObject.fromObject(data);
			outJson(response, commonDao.list(sql).toString());
		} catch (Exception e) {
			outJson(response, "{\"result\":\"0\"}");
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * @param request
	 * @param response
	 * @return
	 * 执行sql
	 */
	@RequestMapping("/executeSql")
	public String executeSql(HttpServletRequest request,HttpServletResponse response){
		try {
//			String data=request.getReader().readLine();
//			JSONObject dataJson=JSONObject.fromObject(data);
//			String sql=dataJson.getString("sql");
			
			String sql=request.getParameter("sql");
			if(commonDao.execute(sql)){
				outJson(response, "{\"result\":\"1\"}");
			}else{
				outJson(response, "{\"result\":\"0\"}");
			}
		} catch (Exception e) {
			outJson(response, "{\"result\":\"0\"}");
			e.printStackTrace();
		}
		return null;
	}
	
	
	/**
	 * 添加任务
	 */
	@RequestMapping("/addJob")
	public String addJob(HttpServletRequest request,HttpServletResponse response){
		try {
			String bean=request.getParameter("bean");
			System.out.println(bean);
			if(commonDao.addJob(bean)){
				outString(response, "1");
			}else{
				outString(response, "0");
			}
		} catch (Exception e) {
			outString(response, "0");
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 删除任务
	 */
	@RequestMapping("/delJob")
	public String delJob(HttpServletRequest request,HttpServletResponse response){
		try {
			String id=request.getParameter("id");
			if(commonDao.delJob(id)){
				outString(response, "1");
			}else{
				outString(response, "0");
			}
		} catch (Exception e) {
			outString(response, "0");
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 删除设备
	 */
	@RequestMapping("/delDevice")
	public String delDevice(HttpServletRequest request,HttpServletResponse response){
		try {
			String id=request.getParameter("id");
			if(commonDao.delDevice(id)){
				outString(response, "1");
			}else{
				outString(response, "0");
			}
		} catch (Exception e) {
			outString(response, "0");
			e.printStackTrace();
		}
		return null;
	}
	
	
	
	
	
	
	
	
	
	/**
	 * @param request
	 * @param response
	 * @return
	 * 往session里添加值
	 */
	@RequestMapping("/setSessionValue")
	public String setSessionValue(HttpServletRequest request,HttpServletResponse response){
		try {
			String name=request.getParameter("name");
			String value=request.getParameter("value");
			request.getSession().setAttribute(name, value);
			outJson(response, "{\"result\":\"1\"}");
		} catch (Exception e) {
			outJson(response, "{\"result\":\"0\"}");
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * @param request
	 * @param response
	 * @return
	 * 获取session里的某个值
	 */
	@RequestMapping("/getSessionValue")
	public String getSessionValue(HttpServletRequest request,HttpServletResponse response){
		try {
			String name=request.getParameter("name");
			outJson(response, "{\"value\":\""+request.getSession().getAttribute(name)+"\"}");
		} catch (Exception e) {
			outJson(response, "{\"result\":\"0\"}");
			e.printStackTrace();
		}
		return null;
	}
	
	
	
	/**
	 * @param request
	 * @param response
	 * @return
	 * 发送get请求
	 */
	@RequestMapping("/sendGetRequest")
	public String sendGetRequest(HttpServletRequest request,HttpServletResponse response){
		try {
			String url=request.getParameter("url");
			outJson(response, HttpUtil.doGet(url));
		} catch (Exception e) {
			outJson(response, "{\"result\":\"0\"}");
			e.printStackTrace();
		}
		return null;
	}

	
	@RequestMapping("/test")
	public String test(HttpServletRequest request,HttpServletResponse response){
		try {
			outJson(response, "{\"success\":\"success\"}");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

		
}
