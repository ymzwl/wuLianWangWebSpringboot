/**
 * 
 */
/**
 * @author Administrator
 *
 */
package app.dao;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import app.util.Log;
import app.util.Util;
import net.sf.json.JSONArray;

@Repository
public class BaseDao{

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public JSONArray list(String sql) {
    	Log.outLog(sql);
    	JSONArray result = new JSONArray();
        try {
        	List<Map<String, Object>> qList = jdbcTemplate.queryForList(sql);
        	result=Util.list_to_JSONArray(qList);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
    public boolean execute(String sql){
    	Log.outLog(sql);
    	boolean result=false;
    	try {
    		int executeR=jdbcTemplate.update(sql);
    		if(executeR>0){
    			result=true;
    		}
		} catch (Exception e) {
			e.printStackTrace();
		}
    	return result;
    }
}