package app.dao;
 
import java.util.List;
 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
 
 
@Repository
public class UserDao extends BaseDao{
	
	/**
	 * 获取全部的用户
	 * @return
	 */
	public JSONArray findAll() {
		String sql="select * from user";
		return super.list(sql);
	}
	
	/**
	 * 添加用户
	 * @param user
	 */
	public void addUser(JSONObject user) {
		String sql="";
		super.execute(sql);
	}
	
	/**
	 * 修改用户
	 * @param user
	 */
	@Transactional
	public void updateUser(JSONObject user) {
		String sql="";
		super.execute(sql);
	}
	
	/**
	 * 删除用户
	 * @param id
	 */
	@Transactional
	public void deleteUser(int id) {
		String sql="";
		super.execute(sql);
	}
	
	/**
	 * 查询指定用户
	 * @param id
	 * @return
	 */
	public JSONObject findById(int id) {
		String sql="";
		return super.list(sql).getJSONObject(0);
	}
}
