package app.job;

import java.io.Serializable;

import javax.annotation.Resource;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;

import app.MqttGateway;
import app.dao.CommonDao;
import net.sf.json.JSONObject;
/**
 * Job 的实例要到该执行它们的时候才会实例化出来。每次 Job 被执行，一个新的 Job 实例会被创建。
 * 其中暗含的意思就是你的 Job 不必担心线程安全性，因为同一时刻仅有一个线程去执行给定 Job 类的实例，甚至是并发执行同一 Job 也是如此。
 * @DisallowConcurrentExecution 保证上一个任务执行完后，再去执行下一个任务，这里的任务是同一个任务
 */
@DisallowConcurrentExecution
public class MyJob implements  Job,Serializable {

	private static final long serialVersionUID = 1L;
	
	@Resource
	private CommonDao commonDao;
	@Autowired
    private MqttGateway mqttGateway;

	
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		System.out.println("任务描述："+context.getJobDetail().getDescription());
		String myjobId=context.getJobDetail().getDescription();
		JSONObject myjob=commonDao.list("select * from myjob where id='"+myjobId+"'").getJSONObject(0);
		int control=myjob.getInt("control");
		String deviceId=myjob.getString("deviceId");
		
		if(control==0) {
			closeSheBei(deviceId);
		}
		if(control==1) {
			openSheBei(deviceId);
		}
	}
	
	/**
	 * 打开设备 
	 */
	//{id:"123",command:0,type:"control"}
	public void openSheBei(String deviceId){
        JSONObject msgJson=new JSONObject();
        msgJson.put("id", deviceId);
        msgJson.put("command", 1);
        msgJson.put("type", "control");
		//sendMqtt(msgJson.toString(),deviceId);
        mqttGateway.sendToMqtt(msgJson.toString(), deviceId);
	}
	
	public void closeSheBei(String deviceId){
		JSONObject msgJson=new JSONObject();
        msgJson.put("id", deviceId);
        msgJson.put("command", 0);
        msgJson.put("type", "control");
		//sendMqtt(msgJson.toString(),deviceId);
		mqttGateway.sendToMqtt(msgJson.toString(), deviceId);
	}
	
	
}
