﻿
(function () {
	var loading='<div id="loading" style="position: fixed;background-color: rgba(0,0,0,0.5);width: 100%;height: 100%;z-index: 99999;top: 0;display:flex;align-items:center;justify-content:center;color: black;font-size: 1.5rem;">'+
							''+
						'</div>';
	/*$.ajax().ajaxSend(function(){
		alert("ajaxSend");
	}).ajaxStart(function(){
		alert("ajaxStart");
	}).ajaxComplete(function(){
		//alert("ajaxComplete");
	}).ajaxSuccess(function(){
		//alert("ajaxSuccess");
	});*/
	
	//全局ajax控制
	var _ajax = $.ajax;
	$.ajax = function(opt) {
		var _opt = opt;
		if (opt.noloading) {
			console.log("opt.noloading");
		} else {
			_opt = $.extend(opt, {
				beforeSend : function(XMLHttpRequest) {
					//console.log("beforeSend");
					$("html").append(loading);
				},
				complete : function(XMLHttpRequest, textStatus) {
				//	console.log("complete");
					$("#loading").remove();
				}
			});
		}
		return _ajax(_opt);
	};
	
})();




/*********************************************************************************************/


var urlConfig="http://148.70.214.205:8080/wuLianWang/"
//var urlConfig="http://192.168.0.13:8080/wuLianWang/"


Date.prototype.format = function(format) {
	var date = {
		"M+" : this.getMonth() + 1,
		"d+" : this.getDate(),
		"h+" : this.getHours(),
		"m+" : this.getMinutes(),
		"s+" : this.getSeconds(),
		"q+" : Math.floor((this.getMonth() + 3) / 3),
		"S+" : this.getMilliseconds()
	};
	if (/(y+)/i.test(format)) {
		format = format.replace(RegExp.$1, (this.getFullYear() + '')
				.substr(4 - RegExp.$1.length));
	}
	for ( var k in date) {
		if (new RegExp("(" + k + ")").test(format)) {
			format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? date[k]
					: ("00" + date[k]).substr(("" + date[k]).length));
		}
	}
	return format;
}

function time_yyyyMMdd_hhmmss(time) {
	return new Date(time).format("yyyy-MM-dd hh:mm:ss");
}
function time_yyyyMMdd_hhmm(time) {
	return new Date(time).format("yyyy-MM-dd hh:mm");
}
function time_yyyyMMdd(time) {
	return new Date(time).format("yyyy-MM-dd");
}
function time_yyyyMM(time) {
	return new Date(time).format("yyyy-MM");
}
function time_yyyy(time) {
	return new Date(time).format("yyyy");
}
function time_MM(time) {
	return new Date(time).format("MM");
}
function time_dd(time) {
	return new Date(time).format("dd");
}
/*
 * 获取当前时间毫秒数
 * */
function timeMillisecond(){
	return new Date().getTime();
}
/**
 * 字符串转时间
 * stringTime 例：2014-07-10 10:21:12
 */
function timeToNumber(stringTime){
	var str = stringTime.replace(/-/g,"/");
	var num =  Date.parse(new Date(str));
	return num;
}
/*
 * 获取当前年
 * */
function getYear(){
	var date = new Date();
	return date .getFullYear(); //获取完整的年份(4位)
}
/*
 * 获取当前月
 * */
function getMonth(){
	var date = new Date();
	return date.getMonth()+1; //获取当前月份(0-11,0代表1月)
}
/*
 * 获取当前日
 * */
function getDate(){
	var date = new Date();
	return date.getDate(); //获取当前月份(0-11,0代表1月)
}

/**
 * 检查字符串是否包含特殊字符 lichao
 * 
 * @param str
 * @returns {Boolean}
 */
function checkSpecialChar(str) {
	var pattern = new RegExp("[`~!@#$^&*()=|{}':;',\\[\\].<>/?~！@#￥……&*（）——|{}【】‘；：”“'。，、？]");
	if (str != "" && str != null) {
		if (pattern.test(str)) {
			return false;
		} else {
			return true;
		}
	}else
	{
		return true;
	}
}
    
/**
 * 检查字符串是否包含关键字 lichao
 * 
 * @param str
 */
function checkKeyWords(str){
	var text = ',」,「,null,boolean,int,long,short,byte,float,double,char,class,interface,if,else,for,switch,default,break,continue,return,public,protect,default,private,final,static,void,strictfp,abstract,transient,synchronized,volatile,native,package,import,throw,throws,extends,new,true,false,null,goto,const,this,implements,super,instanceof,try,volatile,while,';
    if(text.indexOf(','+str.toLowerCase().trim()+',') >= 0||str.indexOf('null')>=0){
    	return true;
    }else
    {
    	return false;
    }
}
/**
 * 检查查询条件，开始时间不能晚于结束时间
 * @param start
 * @param end
 * @returns {Boolean}
 */
function checkSEDate(start,end){
	if(start!=""&&end!=""){
		if(start>end){
			return true;
		}
	}
	return false;
}

/**
 * 检查对象值是否为空
 * jqObj:jq对象
 */
function checkNull(jqObj){
	if(!jqObj.val()){
		jqObj.focus();
		return true;
	}
}

/**
 * 保留3位小数，并且四舍五入
 * @param val
 * @returns
 */
function getDouble3(val){
	if((val+"").indexOf(".")>0){
		var n=Number((val+"").substring(0,(val+"").indexOf(".")+5));
		n=Math.round(n*1000)/1000;
		return n;
	}else{
		return val;
	}
}

/*
 * 获取url参数
 * */
function getUrlParam(name) {
  var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
  var r = window.location.search.substr(1).match(reg);
  if (r != null) return unescape(r[2]);
  return null;
}
/*
 * 往session里添加值
 * */
function setSessionValue(name,value){
	var data={};
	data.name=name;
	data.value=value;
	$.ajax({
      type: 'post',
      async:false,
      url:'common/setSessionValue',
      data:data,
      success: function (data) {
      	//alert(JSON.stringify(data));
      	
      },
      error: function (error) {
        alert(JSON.stringify(error));
      }
    })
}
/*
 * 获取session里的某一个值
 * */
function getSessionValue(name){
	var data={};
	data.name=name;
	var result="";
	$.ajax({
      url:'common/getSessionValue',
      data:data,
      async:false,
      success: function (data) {
      	//alert(JSON.stringify(data));
      	result=data.value;
      },
      error: function (error) {
        alert(JSON.stringify(error));
      }
    })
    return result;
}

function listSql(sql,callBackFun,ifAsync){
	var data={};
	data.sql=sql;
	var async=true;
	if(ifAsync==false){
		async=false;
	}
	$.ajax({
		url:'common/listSql',
		type:"post",
		async:async,
		data:data,
		success: function (data) {
			callBackFun(data);
		},
		error: function (error) {
			alert(JSON.stringify(error));
		}
	})
}


function executeSql(sql,callBackFun,ifAsync){
	
	var data={};
	data.sql=sql;
	var async=true;
	if(ifAsync==false){
		async=false;
	}
	$.ajax({
		url:'common/executeSql',
		type:"post",
		async:async,
		data:data,
		success: function (data) {
			callBackFun(data);
		},
		error: function (error) {
			alert(JSON.stringify(error));
		}
	})
	
	/*
	var xhr=null;
	xhr=new plus.net.XMLHttpRequest();
	xhr.onreadystatechange=function(){
		switch ( xhr.readyState ) {
            case 0:
            	console.log( "xhr请求已初始化" );
            break;
            case 1:
            	console.log( "xhr请求已打开" );
            break;
            case 2:
            	console.log( "xhr请求已发送" );
            break;
            case 3:
                console.log( "xhr请求已响应");
                break;
            case 4:
                console.log( "xhr请求已完成");
                if ( xhr.status == 200 ) {
                	var data=xhr.responseText;
					callBackFun(data);
                } else {
                	console.log( "xhr请求失败："+xhr.status );
                }
                break;
            default :
                break;
        }
	}
	xhr.open( "POST", urlConfig+"common/executeSql.do" );
	var data={};
	data.sql=sql;
	// 发送HTTP请求
	xhr.send(JSON.stringify(data));
	*/
}


function sendPost(url,data,callBackFun){
	var xhr=null;
	xhr=new plus.net.XMLHttpRequest();
	xhr.onreadystatechange=function(){
		switch ( xhr.readyState ) {
            case 0:
            	console.log( "xhr请求已初始化" );
            break;
            case 1:
            	console.log( "xhr请求已打开" );
            break;
            case 2:
            	console.log( "xhr请求已发送" );
            break;
            case 3:
                console.log( "xhr请求已响应");
                break;
            case 4:
                console.log( "xhr请求已完成");
                if ( xhr.status == 200 ) {
                	var data=xhr.responseText;
					callBackFun(data);
                } else {
                	console.log( "xhr请求失败："+xhr.status );
                }
                break;
            default :
                break;
        }
	}
	xhr.open( "POST", url );
	// 发送HTTP请求
	xhr.send(JSON.stringify(data));
}


/*
 * ajax跨域，向任意地址发送get请求，该方法为同步
 * */
function sendGetRequest(url,callBack){
	var data={};
	data.url=url;
	$.ajax({
		type: 'get',
		url:'common/sendGetRequest',
		async: false,
		data:data,
		success: function (data) {
			callBack(data);
		},
		error: function (error) {
			alert(JSON.stringify(error));
		}
	})
}



//用于生成uuid
function uuid() {
	function S4() {
	    return (((1+Math.random())*0x10000)|0).toString(16).substring(1);
	}
	var result=(S4()+S4()+S4()+S4()+S4()+S4()+S4()+S4());
    return result;
}

function uuid16() {
	function S4() {
	    return (((1+Math.random())*0x10000)|0).toString(16).substring(1);
	}
	var result=(S4()+S4()+S4()+S4());
    return result;
}









